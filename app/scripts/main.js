'use strict';

// ready
$(document).ready(function() {

    var body = $('body');

    //date calendar
    $('.date').pickmeup_twitter_bootstrap({
        // position       : 'bottom',
        format: 'd B Y',
        hide_on_select : true
    });
    $('.date-range').pickmeup_twitter_bootstrap({
        // position       : 'bottom',
        mode : 'range',
        format: 'd B Y',
        hide_on_select : true
    });
    //date calendar

    $('.fg-tags ul').generateMenu();

    // anchor
    // $(".anchor").on("click","a", function (event) {
    //     event.preventDefault();
    //     var id  = $(this).attr('href'),
    //         top = $(id).offset().top;
    //     $('body,html').animate({scrollTop: top + 40}, 1000);
    // });
    // anchor

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).toggleClass('nav-is-visible');
        $(this).parents().find('body').toggleClass('active');
        $(this).next().toggleClass('nav-is-visible');
        //$(this).next().slideToggle();
    });
    $('.main-nav__item--parent').click(function () {
        $(this).toggleClass('active');
        //$(this).next().toggleClass('nav-is-visible');
        $(this).find('.sub-nav').slideToggle();
    });
    // adaptive menu

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    $('.carousel-item').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 2,
                    adaptiveHeight: true
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 1,
                    adaptiveHeight: true
                }
            }
        ]
    });
    $('.carousel-item-multiple').slick({
        infinite: true,
        slidesToShow: 11,
        slidesToScroll: 5,
        // centerMode: true,
        responsive: [
            {
                breakpoint: 1310,
                settings: {
                    slidesToShow: 7
                }
            },
            {
                breakpoint: 990,
                settings: {
                    slidesToScroll: 4,
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToScroll: 3,
                    slidesToShow: 3
                }
            }
        ]
    });
    $('.fg-menu--hor--js').slick({
        slidesToShow: 5,
        variableWidth: true,
        infinite: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 1310,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 750,
                settings: {
                    // centerMode: true,
                    // variableWidth: false,
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.slideshow').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        asNavFor: '.slider-nav',
        responsive: [
            {
                breakpoint: 990,
                settings: {
                    arrows: true
                }
            }
        ]
    });
    $('.slider-nav').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        // centerMode: true,
        focusOnSelect: true
    });
    // slider

    // select {select2}
    $('.select-hard').on("select2:select", function(e) {
        var classSelect = $(this).parent().find('.select2');
        var cusSelect = $(this).parent().find('.select2-selection__rendered').attr('title');
        if (  cusSelect == '' ) {
            $(this).removeClass('edited');
            classSelect.removeClass('edited');
        } else {
            $(this).addClass('edited');
            classSelect.addClass('edited');
        }
    });
    $('.label-hard').click( function () {
        $(this).parent().find('.select-hard').select2('open');
    });
    $('select:not(.default-select)').select2({
        minimumResultsForSearch: Infinity
    });
    // select

    // floating labels
    var handleInput = function(el) {
        if (el.val() != "") {
            el.addClass('edited');
        } else {
            el.removeClass('edited');
        }
    };
    body.on('keydown', '.form-control', function(e) {
        handleInput($(this));
    });
    body.on('blur', '.form-control', function(e) {
        handleInput($(this));
    });
    // floating labels


    // range slider
    if ($("#rangeSlider").length) {
        var range = document.getElementById('rangeSlider');
        var start = document.getElementById('start');
        var end = document.getElementById('end');
        noUiSlider.create(range, {
            start: [6078, 23678],
            connect: true,
            step: 10,
            range: {
                'min': 0,
                'max': 35000
            }
        });
        range.noUiSlider.on('update', function (values, handle) {
            var value = values[handle];
            if (handle) {
                end.value = Math.round(value);
            } else {
                start.value = Math.round(value);
            }
        });
        end.addEventListener('change', function () {
            range.noUiSlider.set([this.value, null]);
        });
        start.addEventListener('change', function () {
            range.noUiSlider.set([null, this.value]);
        });
    }
    // range slider

    // $('input[type="radio"]').click(function() {
    //     if($(this).val('ullist').is(':checked')) {
    //         $('#show-me').show();
    //     }
    //     else {
    //         $('#show-me').hide();
    //     }
    // });
    $('.addobject--js input').click(function() {
        if($(this).val('addobject').is(':checked')) {
            $('#show-me').show();
        }
        else {
            $('#show-me').hide();
        }
    });
    $('.radiotab--js').on('click', '.radio input', function() {
        var radTab = $(this).val();
        if(radTab == 'ullist') {
            $(this).closest('.radiotab').find('.delivery-block--trs-1').addClass('active');
            $(this).closest('.radiotab').find('.delivery-block--trs-2').removeClass('active');
        } else {
            $(this).closest('.radiotab').find('.delivery-block--trs-1').removeClass('active');
            $(this).closest('.radiotab').find('.delivery-block--trs-2').addClass('active');
        }
    });

    //fixed header
    $(function(){
        $(window).scroll(function() {
            var top = $(document).scrollTop();
            if (top > 220) $('.header-fixed').addClass('fix');
            else $('.header-fixed').removeClass('fix');
        });
    });
    //fixed header


    //type file
    $('.input__file-js').change(function() {
        $('.input__file-js').each(function() {
            var name = this.value;
            var reWin = /.*\\(.*)/;
            var fileTitle = name.replace(reWin, "$1");
            var reUnix = /.*\/(.*)/;
            fileTitle = fileTitle.replace(reUnix, "$1");
            $('.input__text-js').text('Загружено: '+ fileTitle);
        });
    });
    //type file


    // popup {magnific-popup}
    $('.imageLink').magnificPopup({
        delegate: 'a',
        type:'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 500,
            opener: function(element) {
                return element.find('img');
            }
        }
    });
    $('.popup-modal').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#username',
        closeBtnInside: false
        //modal: true
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    // popup


    //tabs
    $('.tabs__caption').on('click', ':not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    //tabs


    //search
    $('.mobile--btn').click(function () {
       $(this).prev().addClass('active');
    });
    //search

    //close
    $('.close--js').click(function () {
       $(this).parent().parent().hide('');
        return false;
    });
    //close

    //.slider-line--js
    $('.slider-line--js').each(function(){
        var per = $(this).attr('data-per');
        $(this).children().css('width', per + '%');
        if ( per == 101 ) {
            $(this).children().css('background-color','#ff7055');
        } else if ( per > 40 && per <= 90) {
            $(this).children().css('background-color','#ffc338');
        } else if ( per > 70) {
            $(this).children().css('background-color','#33d133');
        } else {
            $(this).children().css('background-color','#ff7055');
        }
    });
    //.slider-line--jsi

    //draft
    $('.draft--js').click(function () {
        $(this).toggleClass('active');
        $('.draft-block--js').slideToggle();
        return false;
    });
    //draft

    //table
    $('.table-title--js').click(function () {
        // $(this).toggleClass('active');
        $(this).parent().parent().toggleClass('active');
        $(this).parent().parent().next().slideToggle();
        return false;
    });
    //draft


    //accordion
    $('.accordion-title').click(function(e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.next().hasClass('active')) {
            $this.next().removeClass('active');
            $this.removeClass('active');
            $this.next().slideUp(350);
        } else {
            $this.parent().parent().find('.accordion-title').removeClass('active');
            $this.toggleClass('active');
            $this.parent().parent().find('.accordion-content').removeClass('active');
            $this.parent().parent().find('.accordion-content').slideUp(350);
            $this.next().toggleClass('active');
            $this.next().slideToggle(350);
        }
    });
    $('.accordion-btn').click(function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.closest('.fg-accordion__section').find('.accordion-title').removeClass('active');
        $this.closest('.fg-accordion__section').find('.accordion-content').removeClass('active');
        $this.closest('.fg-accordion__section').find('.accordion-content').slideToggle(350);
        setTimeout(function() {
            var theOffset = $this.closest('.fg-accordion__section').find('.accordion-title').offset();
            $('body,html').animate({ scrollTop: theOffset.top - 90 });
        }, 310);
    });
    //accordion


    //.slider-line--js
    $('.slider-line--js').each(function(){
        var per = $(this).attr('data-per');
        var sum = $(this).attr('data-sum');
        var childWidth = 100 - per;
        var num = sum * childWidth / 100;
        $(this).find('.bar-children').css('width', childWidth + '%');
        $(this).find('.bar-txt').text(per + '%');
        if ( per < 100 ) {
            $(this).find('.bar-info').html('Осталось купить <br> на ' + num + ' руб.');
        }
    });
    //.slider-line--jsi

    $('[data-toggle="tooltip"]').tooltip();

    //tags
    $('.fg-tags--js').click(function () {
        $(this).parent().parent().remove();
    });
    //tags

    //.show--js
    $('.show--js').click(function () {
        $(this).toggleClass('active');
        $(this).nextAll().slideToggle();
        return false;
    });
    $('.show--all').click(function () {
        $(this).hide();
        $(this).parent().parent().find('.hide--all').show('slow');
        return false;
    });
    //.show--js


    // autocomplete
    var options = {
        data: [
            {"name": "Адыгея"},
            {"name": "Архангельская обл."},
            {"name": "Астраханская обл."},
            {"name": "Башкортостан"},
            {"name": "Белгородская обл."},
            {"name": "Брянская обл."},
            {"name": "Владимирская обл."},
            {"name": "Волгоградская обл."},
            {"name": "Вологодская обл."},
            {"name": "Воронежская обл."},
            {"name": "Дагестан"},
            {"name": "Ивановская обл."},
            {"name": "Ингушетия"},
            {"name": "Кабардино-Балкария"},
            {"name": "Калининградская обл."}
        ],
        getValue: "name",
        list: {
            match: {
                enabled: true
            }
        },
        template: {
            type: "custom",
            method: function(value, item) {
                //return "<span class='flag flag-" + (item.img).toLowerCase() + "' ></span>" + value;
                return "<div class='row'>" + value + "</div>";
            }
        }
    };
    $("#search").easyAutocomplete(options);
    // autocomplete


    $(function() {
        return $("[data-sticky_column]").stick_in_parent({
            parent: "[data-sticky_parent]",
            offset_top: 90
        });
    });
});
// ready

$.fn.generateMenu = function(options) {
    options = $.extend({
        more : 100
    }, options);
    return this.each(function() {
        var $nav = $(this);
        var $li = $nav.children('li');
        var navWidth = $nav.width();
        var maxWidth = navWidth - options.more;
        var $cloneLi = [];
        var $liVisible = []; //li in visible area
        var $liHidden = []; //li in more menu
        var visibleLiWidth = 0;
        $nav.generate = true;

        $li.each(function() {
            var $this = $(this);
            $cloneLi.push({
                'li' : $this,
                'width' : $this.outerWidth(true)
            });
        });

        function generateMenuElements() {
            var $liVisible = [];
            var $liHidden = [];
            var visibleLiWidth = 0;
            var navWidth = $nav.width();
            var maxWidth = navWidth - options.more;

            for (var i=0; i<$cloneLi.length; i++) {
                var $this = $cloneLi[i];
                visibleLiWidth += $this['width'];
                if (visibleLiWidth > maxWidth) {
                    $liHidden.push($this.li);
                } else {
                    $liVisible.push($this.li);
                }
            }

            if ($liHidden.length) {
                $nav.empty();
                for (var i=0; i<$liVisible.length; i++) {
                    $nav.append($liVisible[i]);
                }
                $nav.append($('<li class="more"><div class="btn__icon--center fg-tags__item "><i class="icon icon-30 icon--more"></i></div></li>'));

                var $ulMore = $('<ul class="more-menu"></ul>');

                for (var i=0; i<$liHidden.length; i++) {
                    $ulMore.append($liHidden[i]);
                }

                $nav.find('li.more').append($ulMore);

            }
        }

        function resetMenu() {
            $nav.empty();
            for (var i=0; i<$cloneLi.length; i++) {
                $nav.append($cloneLi[i].li);
            }
        }

        $nav.on('generateMenuOff', function() {
            $nav.generate = false;
        });
        $nav.on('generateMenuOn', function() {
            $nav.generate = true;
            generateMenuElements();
        });
        $nav.on('resetMenu', function() {
            resetMenu();
        });

        generateMenuElements();


        $(window).resize(function() {
            if ($nav.generate) {
                generateMenuElements();
            }
        });
    });
};

//ymaps
ymaps.ready(init);
function init(){
    var myMap;
    var center = [55.76, 37.64];
    myMap = new ymaps.Map("mapid", {
        center: center,
        zoom: 15
    });
    var myPlacemark = new ymaps.Placemark(center , {},
        {
            iconLayout: 'default#image',
            iconImageHref: 'images/location-ico.png',
            balloonContentBodyLayout:'my#theaterlayout',
            balloonMaxWidth: 260
        });
    var myBalloonLayout = ymaps.templateLayoutFactory.createClass(
        '<p><b>Склад АО Форум Нева</b><br>196105, г. Cанкт-Петербург, ул. Благодатная, д. 67 </p>' +
        '<ul class="list--unstyled text__xs"><li><li><span><i class="icon icon-23 icon--phone"></i> (495) 649-69-71</span></li><li><span><i class="icon icon-23 icon--mail"></i> contact@forumgroup.ru</span></li><li><span><i class="icon icon-15-lk icon--lk-time"></i> Пн-Пт с 9 до 22</span></li> </ul>'
    );
    ymaps.layout.storage.add('my#theaterlayout', myBalloonLayout);
    myMap.geoObjects.add(myPlacemark);
}
//ymaps


// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts